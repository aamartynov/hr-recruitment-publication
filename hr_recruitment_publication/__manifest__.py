# -*- coding: utf-8 -*-

{
    'name': 'HR Requirment Publication',
    'summary': 'Integrated HR module with exteranl systems',
    'description': """
Integrated HR module with exteranl systems
==========================================
    """,
    'version': '10.0.0.0.0',
    'category': 'HR',
    'author': "Alexander Martynov",
    'license': 'AGPL-3',
    'depends': ['hr', 'hr_recruitment'],
    'data': [
        'views/hr.xml',
        'data/hr_applicant_collector.xml',
        'security/ir.model.access.csv'
    ],
    'installable': True,
    'auto_install': False
}
