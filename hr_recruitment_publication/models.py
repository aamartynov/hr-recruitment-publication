"""
Resume:
    :name: str
    :age: str
    :email: str
    :phone: str
    :phones: list[str]
    :sex: str
    :birth_date: str
    :department: str
    :department: str
    :department: str
    :department: str
    :source: str
    :salary: str
    :url: str
    :attachments: list[Resume.Attachment]

ResumeAttachment:
    name: str
    data: str
"""

# NOTE:
# after reinstall this module fields
# hr.applicant.job_publication_id.hr_resource is False
# hr.applicant.external_id is False
# and module will make dublicate records!

from odoo import models, fields, api
import logging
from .helper import namedtuple


logger = logging.getLogger(__name__)


Resume = namedtuple('Resume', [
    'id',
    'name',
    'age',
    'email',
    'phone',
    'phones',
    'photo',
    'sex',
    'description',
    'birth_date',
    'department',
    'source',
    'salary',
    'url',
    'attachments'
], default=lambda: None)

Resume.Attachment = namedtuple('ResumeAttachment', [
    'name',
    'data'
])

class HrRecuirmentException(Exception): pass

class HrJob(models.Model):
    _inherit = "hr.job"

    job_publication_ids = fields.One2many("hr_recruitment_publication.job_publication",
        "hr_job_id", string="Job publication", auto_join=True)

    @api.multi
    def load_resumes(self):
        """
        NOTE: this method will be create hr.applicant for all resumes
        wich were get from `job_publication.load_resumes` and this why we must check for duplicate
        with `job_publication._is_duplicate_resume` method
        """

        for hr_job in self:
            for job_publication in hr_job.job_publication_ids:
                new_resumes_count = 0
                logger.info('will load resumes for resource [%s.%s] from job (%s)' % (
                    job_publication.hr_resource,
                    job_publication.id,
                    hr_job.name
                ))

                try:
                    resumes = job_publication.load_resumes() or []
                except HrRecuirmentException:
                    logger.exception('Continue from exception')
                    continue

                for resume in resumes:

                    new_hr_applicant = self.env["hr.applicant"].sudo().create({
                        'name': resume.name,
                        'partner_name': resume.name,
                        'email_from': resume.email,
                        'partner_phone': resume.phone,
                        'job_id': hr_job.id,
                        'external_id': resume.id,
                        'job_publication_id': job_publication.id,
                        'salary_expected': resume.salary,
                        'description': resume.description,
                        'photo': resume.photo
                    })

                    new_resumes_count += 1

                    if resume.attachments is not None:
                        for attachment in resume.attachments:
                            self.env['ir.attachment'].sudo().create({
                                "res_model": "hr.applicant",
                                "res_id": new_hr_applicant.id,
                                "datas": attachment.data,
                                "datas_fname": attachment.name,
                                "name": attachment.name
                            })
            
                logger.info('was load resumes for resource [%s.%s] from job (%s) count %s' % (
                    job_publication.hr_resource,
                    job_publication.id,
                    hr_job.name,
                    new_resumes_count
                ))

    @api.multi
    def load_from_external_resources(self):
        """ For call by ir.crone """
        logger.info('start crone job for hr_recuirment_publication')
        self.search([]).load_resumes()


class JobPublication(models.Model):
    _name = "hr_recruitment_publication.job_publication"

    hr_job_id = fields.Integer('Related job ID', index=True)
    date_publication = fields.Datetime(string="Publication Date")

    hr_resource = fields.Selection(
        selection=lambda _: _._hr_resource(),
        string='HR resource'
    )

    vacancy_url = fields.Char(string="Vacancy URL")
    vacancy_id = fields.Char(string="Vacancy ID")
    archive = fields.Boolean(string="Archive")

    @api.multi
    def load_resumes(self):
        """ Is API method will lookup <specific>_load_resumes """

        return self._execute_specific('_load_resumes')

    @api.model
    def _execute_specific(self, specific_name, args=None):
        """
        :specific_name: str
        :args: list
        """

        args = args or []

        return getattr(
            self,
            str(self.hr_resource) + specific_name,
            lambda *args, **kwargs: None
        )(*args)

    @api.model
    def _hr_resource(self):
        """ For extends other modules """
        return []

    def _is_duplicate_resume(self, resume_external_id):

        search_count = self.env["hr.applicant"].sudo().search_count([
            '&', '&',
            ('external_id', '=', resume_external_id),
            ('job_publication_id.hr_resource', '=', self.hr_resource),
            ('job_id', '=', self.hr_job_id)
        ])

        return search_count > 0


class HrApplicant(models.Model):
    _inherit = 'hr.applicant'

    external_id = fields.Char(string='External ID')
    photo = fields.Binary(string="Photo")
    
    job_publication_id = fields.Many2one(
        "hr_recruitment_publication.job_publication", string="Reference to JobPublication definition")
