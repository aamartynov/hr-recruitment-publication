from collections import namedtuple as basenamedtuple

def namedtuple(typename, field_names, verbose=False, rename=False, default=None):
    Namedtuple = basenamedtuple(typename, field_names, verbose, rename)

    if default is not None:
        if not callable(default):
            raise TypeError('default argument mustbe callable')

        Namedtuple.__new__.__defaults__ = (default(),) * len(Namedtuple._fields)
        
    return Namedtuple
