# -*- coding: utf-8 -*-

import logging


logger = logging.getLogger(__name__)


def plural_form(number, after):
    cases = 2, 0, 1, 1, 1, 2
    
    try:
        number = int(number)
    except ValueError:
        return

    return after[2 if (number % 100 > 4 and number % 100 < 20) else cases[min(number % 10, 5)]]


def fixed(value, char, size):
    value = unicode(value)
    return (size - len(value)) * unicode(char) + value


def months_to_years(value, and_case, month_cases, year_cases):
    try:
        value = int(value)
    except ValueError:
        return
    
    years = value // 12
    months = value % 12
    years_lable = plural_form(years, year_cases)
    months_lable = plural_form(months, month_cases)

    years_chunk = u'{} {}'.format(years, years_lable) if years > 0 else u''
    months_chunk = u'{} {}'.format(months, months_lable) if months > 0 else u''
    and_chunk = u' %s ' % and_case if years_chunk and months_chunk else u''

    return years_chunk + and_chunk + months_chunk
