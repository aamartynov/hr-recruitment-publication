# -*- coding: utf-8 -*-

from odoo import models, api
import logging
import requests
from requests.exceptions import ConnectionError
from os.path import join
from math import ceil
from collections import namedtuple
from odoo.addons.hr_recruitment_publication.models import Resume, HrRecuirmentException
from time import time
from concurrent.futures import ThreadPoolExecutor
import base64
import mimetypes
import jinja2
import os
from . import jinjatools


# "SFMono-Regular", Consolas, "Liberation Mono", Menlo, Courier, monospace


logger = logging.getLogger(__name__)
dir_path = os.path.dirname(os.path.realpath(__file__))
templates_path = '%s/template' % dir_path
mimetype_extension = {_2: _1 for _1, _2 in mimetypes.types_map.items()}

jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(templates_path))

# register custom filters
jinja_environment.filters.update(
    months_to_years=jinjatools.months_to_years,
    plural_form=jinjatools.plural_form,
    fixed=jinjatools.fixed,
)

AuthorizationResponse = namedtuple('AuthorizationResponse', [
    'access_token',
    'refresh_token',
    'ttl',
    'expires_in',
    'token_type'
])

AuthenticationParameters = namedtuple('AuthenticationParameters', [
    'client_id',
    'client_secret',
    'user_login',
    'user_password'
])

# SUPERJOB API
class SuperjobPublication(models.Model):
    _inherit = "hr_recruitment_publication.job_publication"

    _PARAMETERS_KEY_PREFIX = 'hr_recruitment_publication_superjob'
    # 100 is max for superjob
    _OBJECTS_PER_LIST_PAGE = 100
    _MAX_REQUEST_PER_LIST = 50
    _EMPTY_SYMBOLS = '.', None, False, '', u''


    @api.model
    def _hr_resource(self):
        return super(SuperjobPublication, self)._hr_resource() + [('superjob', 'superjob')]

    ########################################
    # Hight level superjob API implmentati #
    ########################################

    @api.multi
    def _superjob_is_configured(self):
        api_host = self._superjob_get_parameter('hr_recruitment_publication_superjob.api_host')
        user_login = self._superjob_get_parameter('hr_recruitment_publication_superjob.user_login')
        user_password = self._superjob_get_parameter('hr_recruitment_publication_superjob.user_password')
        client_id = self._superjob_get_parameter('hr_recruitment_publication_superjob.client_id')
        client_secret = self._superjob_get_parameter('hr_recruitment_publication_superjob.client_secret')

        for parameter in api_host, user_login, user_password, client_id, client_secret:
            if parameter in self._EMPTY_SYMBOLS:
                return False

        return True

    @api.multi
    def superjob_load_resumes(self):
        """ Specific enterpoint for method `load_resumes` """

        if not self._superjob_is_configured() or not self.superjob_is_vacancy_exist():
            logger.info('superjob resumes loading passed')
            return None

        logger.info('check vacancy with id %s in superjob' % self.vacancy_id)

        resumes_response = self._superjob_do_request_for_list('2.0/resumes/received/%s' % self.vacancy_id)
        objects = resumes_response['objects']
        resume_template = jinja_environment.get_template('resume.html')

        logger.info('superjob resumes received: %s for vacancy: %s' % (
            len(objects),
            self.vacancy_id
        ))

        objects_futures = []
        resumes = []

        with ThreadPoolExecutor(max_workers=5) as executor:
            for objects_item in objects:

                if self._is_duplicate_resume(objects_item['resume']['id']):
                    continue

                photo_link = objects_item['resume']['photo']

                if photo_link:
                    future_for_photo = executor.submit(requests.get, photo_link, stream=True)
                else:
                    future_for_photo = None

                objects_futures.append((objects_item, future_for_photo))

            for objects_item, future_for_photo in objects_futures:

                photo_base64 = None

                if future_for_photo is not None:
                    try:
                        response = future_for_photo.result()
                    except Exception as exception:
                        logger.error('error while get photo %s' % str(exception))
                    else:
                        if response.status_code == requests.codes.ok:
                            response.raw.decode_content = True
                            photo_base64 = base64.b64encode(response.raw.read())

                resumes.append(Resume(
                    id=objects_item['resume']['id'],
                    name=objects_item['resume']['name'],
                    age=objects_item['resume']['age'],
                    email=objects_item['resume']['email'],
                    phone=objects_item['resume']['phone1'],
                    salary=objects_item['resume']['payment'],
                    description=resume_template.render(resume=objects_item['resume']),
                    photo=photo_base64
                ))

        return resumes

    @api.multi
    def superjob_is_vacancy_exist(self):
        
        if not self.vacancy_id:
            return False

        try:
            self._superjob_do_request('/2.0/vacancies/%s' % self.vacancy_id)
        except SuperjobApiCallNotFoundError:
            logger.info('vacancy with id %s not found in superjob' % self.vacancy_id)
            return False

        return True

    ########################################
    # Low level superjob API implmentation #
    ########################################

    def _superjob_get_access_token(self):
        access_token = self._superjob_get_parameter('hr_recruitment_publication_superjob.access_token')
        access_token_ttl = self._superjob_get_parameter('hr_recruitment_publication_superjob.access_token_ttl')

        # do authentication for get access token
        if access_token in self._EMPTY_SYMBOLS:
            # get authentication parameters
            user_login = self._superjob_get_parameter('hr_recruitment_publication_superjob.user_login')
            user_password = self._superjob_get_parameter('hr_recruitment_publication_superjob.user_password')
            client_id = self._superjob_get_parameter('hr_recruitment_publication_superjob.client_id')
            client_secret = self._superjob_get_parameter('hr_recruitment_publication_superjob.client_secret')

            # get authorization parameters from superjob
            authorization = self._superjob_auth_by_password(user_login, user_password, client_id, client_secret)

            # save authentication parameters in odoo
            self._superjob_set_authorization_parameters(authorization)

            access_token = authorization.access_token

        # do refresh access token
        elif access_token_ttl in self._EMPTY_SYMBOLS or time() >= float(access_token_ttl):
            logger.info("token inspired time=%s ttl=%s" % (int(time()), access_token_ttl))
            
            refresh_token = self._superjob_get_parameter('hr_recruitment_publication_superjob.refresh_token')
            client_id = self._superjob_get_parameter('hr_recruitment_publication_superjob.client_id')
            client_secret = self._superjob_get_parameter('hr_recruitment_publication_superjob.client_secret')
            
            authorization = self._superjob_refresh_access_token(refresh_token, client_id, client_secret)

            # save authentication parameters in odoo
            self._superjob_set_authorization_parameters(authorization)

            access_token = authorization.access_token

        return access_token

    def _superjob_set_authorization_parameters(self, authorization):
        self._superjob_set_parameter('hr_recruitment_publication_superjob.access_token', authorization.access_token)
        self._superjob_set_parameter('hr_recruitment_publication_superjob.access_token_ttl', authorization.ttl)
        self._superjob_set_parameter('hr_recruitment_publication_superjob.refresh_token', authorization.refresh_token)
        self._superjob_set_parameter('hr_recruitment_publication_superjob.expires_in', authorization.expires_in)
        self._superjob_set_parameter('hr_recruitment_publication_superjob.token_type', authorization.token_type)
        self._superjob_set_parameter('hr_recruitment_publication_superjob.access_token', authorization.access_token)
    
    def _superjob_set_parameter(self, key, value):
        return self.env['ir.config_parameter'].search([('key', '=', key)], limit=1).sudo().write({'value': value})

    def _superjob_get_parameter(self, key):
        return self.env['ir.config_parameter'].search([('key', '=', key)], limit=1).value

    def _superjob_make_method_url(self, *paths):
        api_host = self._superjob_get_parameter('hr_recruitment_publication_superjob.api_host')
        return join(api_host, *(_.strip('/') for _ in paths))

    def _superjob_do_request(self, path, params={}, headers={}, method='get', use_auth=True):
        headers['X-Api-App-Id'] = self._superjob_get_parameter('hr_recruitment_publication_superjob.client_secret')

        if use_auth:
            headers['Authorization'] = 'Bearer %s' % self._superjob_get_access_token()

        logger.info('call method [%s] for Superjob API' % path)

        try:
            response = requests.request(method, self._superjob_make_method_url(path), params=params, headers=headers)
            return self._supperjob_do_validate_response(response.json())
        except ConnectionError as error:
            raise HrRecuirmentException(error)
        except SuperjobApiCallError as error:
            logger.error('error {type} ({code}) while do request on superjob api'.format(
                type=type(error).__name__,
                code=error.code
            ))

            raise error

    def _superjob_do_request_for_list(self, path, params={}, headers={}, method='get', use_auth=True):

        params.update(
            page=0,
            count=self._OBJECTS_PER_LIST_PAGE
        )

        response = self._superjob_do_request(path, params, headers, 'get', use_auth)
        total_objects = response['total']
        total_pages = int(ceil(float(total_objects) / float(self._OBJECTS_PER_LIST_PAGE)))

        if total_pages > self._MAX_REQUEST_PER_LIST:
            total_pages = self._MAX_REQUEST_PER_LIST
            
            logger.info('total pages for list is %s and will truncate until %s' % (total_pages, self._MAX_REQUEST_PER_LIST))

        for page in range(1, total_pages):
            response['objects'] += self._superjob_do_request(path, dict(params, page=page), headers, 'get', use_auth)['objects']
        
        return response
    
    def _supperjob_do_validate_response(self, response):

        error = response.get('error', None)

        if error:
            code = error.get('code', None)
            message = error.get('message', '')

            logger.info('recieve error response with code %s' % code)

            if isinstance(message, unicode):
                message = message.encode('utf-8')
            elif isinstance(message, dict):
                message = str(message)

            for ErrorType in (SuperjobApiCallIncorrectParametersError,
                              SuperjobApiCallNotAuthorizedError,
                              SuperjobApiCallAccessError,
                              SuperjobApiCallEntityExpiredError,
                              SuperjobApiCallNotFoundError):

                if ErrorType._code == code:
                    error = ErrorType(message)
                else:
                    error = SuperjobApiCallUndefinedError(message, code)
            
            raise error

        return response


    def _superjob_auth_by_password(self, login, password, client_id, client_secret):

        logger.info((
            'superjob authentication - '
            'login: {login}, '
            'password: {password}, '
            'client_id: {client_id} '
        ).format(
            login=login,
            password=password,
            client_id=client_id
        ))

        response = self._superjob_do_request('2.0/oauth2/password', {
            'login': login,
            'password': password,
            'client_id': client_id,
            'client_secret': client_secret,
            'hr': 1
        }, use_auth=False)

        logger.info('superjob success authentication')

        return AuthorizationResponse(
            access_token=response['access_token'],
            refresh_token=response['refresh_token'],
            ttl=response['ttl'],
            expires_in=response['expires_in'],
            token_type=response['token_type']
        )

    def _superjob_refresh_access_token(self, refresh_token, client_id, client_secret):
        response = self._superjob_do_request('2.0/oauth2/refresh_token', {
            'refresh_token': refresh_token,
            'client_id': client_id,
            'client_secret': client_secret
        }, use_auth=False)

        logger.info('superjob success refresh token')

        return AuthorizationResponse(
            access_token=response['access_token'],
            refresh_token=response['refresh_token'],
            ttl=response['ttl'],
            expires_in=response['expires_in'],
            token_type=response['token_type']
        )


class SuperjobApiError(HrRecuirmentException):
    pass

class SuperjobApiCallError(SuperjobApiError):
    _code = None
    _message = None
    _description = None

    def __init__(self, message, code=None, description=None):
        self._message = message
        self._code = code or self._code
        self._description = description or self._description

        super(SuperjobApiCallError, self).__init__(message)

    @property
    def code(self):
        return self._code
    
    @property
    def message(self):
        return self._message

    @property
    def description(self):
        return self._description


class SuperjobApiCallIncorrectParametersError(SuperjobApiCallError):
    _code = 400

    def __init__(self, message):
        super(SuperjobApiCallIncorrectParametersError, self).__init__(message, description='Parameters are incorrectly passed')


class SuperjobApiCallNotAuthorizedError(SuperjobApiCallError):
    _code = 401

    def __init__(self, message):
        super(SuperjobApiCallNotAuthorizedError, self).__init__(message, description='Authorization required')


class SuperjobApiCallAccessError(SuperjobApiCallError):
    _code = 403

    def __init__(self, message):
        super(SuperjobApiCallAccessError, self).__init__(message, description='Access is denied. Access errors')


class SuperjobApiCallEntityExpiredError(SuperjobApiCallError):
    _code = 410

    def __init__(self, message):
        super(SuperjobApiCallEntityExpiredError, self).__init__(message, description='Expired')


class SuperjobApiCallNotFoundError(SuperjobApiCallError):
    _code = 404
    
    def __init__(self, message):
        super(SuperjobApiCallNotFoundError, self).__init__(message, description='Entity not found')


class SuperjobApiCallUndefinedError(SuperjobApiCallError): pass
