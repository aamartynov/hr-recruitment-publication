# -*- coding: utf-8 -*-

{
    'name': 'HR Requirment Publication Superjob',
    'summary': 'Integrated HR module with Superjob',
    'description': """
Integrated HR module with Superjob
==================================
    """,
    'version': '10.0.0.0.0',
    'category': 'HR',
    'author': "Alexander Martynov",
    'license': 'AGPL-3',
    "depends": ['hr_recruitment_publication'],
    'external_dependencies': {'python': ['concurrent.futures']},
    'data': [
        'data/ir_config_parameter.xml'
    ],
    'installable': True,
    'auto_install': False
}
